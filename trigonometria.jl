# MAC0110 - MiniEP7
# Bruno Hideki Akamine - 11796322

function sin(x)
	sin=big(x)
	for i in 1:10
		k=1+(2*i)
		sin+=((-1)^i)*((x^big(k))/factorial(big(k)))
	end
	return sin
end
function cos(x)
	cos=1
	for i in 1:10
		cos+=((-1)^i)*((x^big(2*i))/factorial(big(2*i)))
	end
	return cos
end

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end
function tan(x)
	tan=0
	x=big(x)
	for i in 1:10
		k=big(2*i)
		tan+=((2^k)*((2^k)-1)*(bernoulli(i))*(x^(k-1)))/factorial(k)
	end
	return tan
end

function quaseigual(v1,v2,erro)
	if abs(v1-v2)<=erro
		return true
	else
		return false
	end
end

function check_sin(value,x)
	if quaseigual(value,sin(x),0.0001)
		return true
	else
		return false
	end
end
function check_cos(value,x)
	if quaseigual(value,cos(x),0.0001)
		return true
	else
		return false
	end
end
function check_tan(value,x)
	if quaseigual(value,tan(x),0.001)
		return true
	else
		return false
	end
end


function taylor_sin(x)
        sin=big(x)
        for i in 1:10
                k=1+(2*i)
                sin+=((-1)^i)*((x^big(k))/factorial(big(k)))
        end
        return sin
end
function taylor_cos(x)
        cos=1
        for i in 1:10
                cos+=((-1)^i)*((x^big(2*i))/factorial(big(2*i)))
        end
        return cos
end
function taylor_tan(x)
        tan=0
        x=big(x)
        for i in 1:10
                k=big(2*i)
                tan+=((2^k)*((2^k)-1)*(bernoulli(i))*(x^(k-1)))/factorial(k)
        end
        return tan
end


using Test
function test()
	println("Iniciando testes da parte 1...")
	println("Iniciando testes com sin(x)")
	@test abs(sin(pi)-0)<=0.0001
	@test abs(sin(pi/2)-1)<=0.0001
	@test abs(sin(pi/3)-0.8660)<=0.0001
	@test abs(sin(pi/4)-0.7071)<=0.0001
	@test abs(sin(pi/6)-0.5)<=0.0001
	println("Fim dos testes com sin(x)")
	println("Iniciando testes com cos(x)")
	@test abs(cos(pi)-(-1))<=0.0001
	@test abs(cos(0)-1)<=0.0001
	@test abs(cos(pi/2)-0)<=0.0001
	@test abs(cos(pi/3)-0.5)<=0.0001
	@test abs(cos(pi/4)-0.7071)<=0.0001
	@test abs(cos(pi/6)-0.8660)<=0.0001
	println("Fim dos testes com cos(x)")
	println("Iniciando testes com tan(x)")
	@test abs(tan(0)-0)<=0.001
	@test abs(tan(pi/6)-0.5774)<=0.001
	@test abs(tan(pi/4)-1)<=0.001
	@test abs(tan(pi/3)-1.7321)<=0.001
	println("Fim dos testes com tan(x)")
	println("Fim dos testes da parte 1")
	println("Inciando testes da parte 2...")
	println("Iniciando testes com check_sin(value,x)")
	@test check_sin(0.5,pi/6)
	@test check_sin(0.7071,pi/4)
	@test !check_sin(2,pi/5)
	@test !check_sin(42,pi)
	@test check_sin(0,pi)
	println("Fim dos testes com check_sin(value,x)")
	println("Iniciando testes com check_cos(value,x)")
	@test check_cos(0.5,pi/3)
	@test check_cos(0.7071,pi/4)
	@test !check_cos(42,pi/7)
	@test !check_cos(0.5,pi/6)
	@test check_cos(0.8660,pi/6)
	@test check_cos(-1,pi)
	println("Fim dos testes com check_cos(value,x)")
	println("Iniando testes com check_tan(value,x)")
	@test check_tan(1,pi/4)
	@test check_tan(0,0)
	@test check_tan(0.5774,pi/6)
	@test !check_tan(12,pi/3)
	@test !check_tan(1.6,pi/3)
	@test check_tan(1.7321,pi/3)
	println("Fim dos teste com check_tan(value,x)")
	println("Fim dos testes da parte 2")
	println("Fim dos testes.")
end
test()

